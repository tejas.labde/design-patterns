//writing an centralised object which will be used for object creation
function Developer(name){
    this.name=name;
    this.type="Developer"
}

function Tester(name){
    this.name=name;
    this.type="Tester"
}

function employeeFactory(){
    this.create=(name,type)=>{
        switch(type){
            case '1': return new Developer(name)
            break;
            case '2':return new Tester(name)
            break;
        }
    }
}

function say(){
    console.log('Hi,i am ' + this.name +' and I am a' +this.type);
}

const emp=new employeeFactory();
const employees=[]

employees.push(emp.create('tejas',1))
employees.push(emp.create('john',2))
employees.push(emp.create('tim',2))
employees.push(emp.create('david',2))

employees.forEach(emp=>{
    say.call(emp)
})


