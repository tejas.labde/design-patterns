//using a proxy object to access other objects. to build a program to get values of cryptocurrencies
function cryptocurrencyAPI(){
    this.getValue=function(coin){
        console.log('calling external api...');
        switch(coin){
            case 'bitcoin':return '8500';
            case 'doge':return '50';
            case 'ethereum':return '175';
            
        }
    }
}

// const api=new cryptocurrencyAPI()
// console.log(api.getValue('bitcoin'));
// console.log(api.getValue('doge'));
// console.log(api.getValue('ethereum'));

 function cryptoProxy(){
     this.api=new cryptocurrencyAPI()
     this.cache={}
     this.getValue=function(coin){
        if(this.cache[coin]==null){
            this.cache[coin]=this.api.getValue(coin)
        }
        return this.cache[coin]

     }
 }

 const proxy=new cryptoProxy()
 console.log(proxy.getValue('bitcoin'))
 console.log(proxy.getValue('doge'))
 console.log(proxy.getValue('ethereum'))
//this set of console.log will not make a request in the network but will take details from the cache object
 console.log(proxy.getValue('bitcoin'))
 console.log(proxy.getValue('doge'))
 console.log(proxy.getValue('ethereum'))
 