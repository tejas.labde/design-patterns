//interaction between multiple objects using one single object like a chatroom object with 
//users talking through it than interacting themselves with each entity


function member(name){
    this.name=name;
    this.chatroom=null;
}

member.prototype={
    send:function(msg,toMember){
        this.chatroom.send(msg,this,toMember)
    },
    receive:function(msg,fromMember){
        console.log(`${fromMember.name} to${this.name}:${msg}`)
    }
}

function chatroom(){
    this.members={}
}

chatroom.prototype={
    addMember:function(memberfn){
        this.members[memberfn.name]=memberfn
        member.chatroom=this
    },
    send:function(msg,fromMember,toMember){
        toMember.receive(msg,fromMember)
    }
}

const chat=new chatroom;
const bobObj=new member('bob')
const johnObj=new member('john')
const timObj=new member('tim')
chat.addMember(bobObj)
chat.addMember(johnObj)
chat.addMember(timObj)
bobObj.send('hey john',johnObj)
johnObj.send('whats up bob',bobObj)
timObj.send('john are u fine',johnObj)