//used to swap strategies in and out for each other very easily
function fedex(){
    this.calculate=package=>{
        return 2.45
    }
}

function ups(){
    this.calculate=package=>{
        return 1.65
    }
}

function usps(){
    this.calculate=package=>{
        return 4.5
    }
}



function shipping(){
    this.company=""
    this.setStrategy=(company)=>{
        this.company=company
    }
    this.calculate=package=>{
        return this.company.calculate(package)
    }
}

const fed=new fedex();
const ups1=new ups();
const usps1=new usps();
const package={from:"Alabama",to:"Georgia",weight:1.56}

// fed.calculate(package);
// ups1.calculate(package);
// usps1.calculate(package);

const shippingObj=new shipping();
shippingObj.setStrategy(fed)
console.log('fedex: ' +shippingObj.calculate(package));
shippingObj.setStrategy(ups1)
console.log('ups: '+shippingObj.calculate(package));
shippingObj.setStrategy(usps1)
console.log('usps: '+shippingObj.calculate(package));
