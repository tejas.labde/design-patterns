//to traverse over a collection of objects
const items=[1,'max',false,'red bull racing',24]

function iterator(items){
    this.items=items;
    this.index=0;
}

iterator.prototype={
    hasNext: function(){
        return this.index < this.items.length
    },
    next: function(){
        return this.items[this.index++]
    }
}
const iteratorObj=new iterator(items);
// console.log(iteratorObj.next());
// console.log(iteratorObj.hasNext());
while(iteratorObj.hasNext()){
    console.log(iteratorObj.next())
}